import { Injectable } from '@angular/core';
import { Message } from '../Models/Message';
import { Account } from '../Models/Account';
import { GroupDiscussion } from '../Models/GroupDiscoussion';
import { Observable, of, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  connectedUser: Account;

  message: string;

  private currentChat = new Subject<any>();


  allChat: GroupDiscussion[] = [
    new GroupDiscussion("test01", [
      new Message("testeur01", "test"),
      new Message("testeur02", "test"),
      new Message("testeur03", "test")]),
    new GroupDiscussion("test02", [
      new Message("testeur04", "test"),
      new Message("testeur05", "test"),
      new Message("testeur06", "test")])];

  uri = 'http://localhost:4000/GroupDiscussion';

  constructor(private http: HttpClient) { }

  addGroup(group_name, group_messages) {
    const obj = {
      name: group_name,
      messages: group_messages
    };
    console.log(obj);
    this.http.post(`${this.uri}/add`, obj).subscribe(res => console.log('done'));
  }

  sendCurrentGroup(name: string) {
    this.currentChat.next({ text: name });
    //console.log("name:" + name);
    //this.getCurrentGroup().subscribe(data => { this.message = data })
    console.log(this.message)
  }

  clearCurrentGroup() {
    this.currentChat.next();
  }

  getCurrentGroup(): Observable<any> {
    return this.currentChat.asObservable();
  } 

  getGroup() {
    return this
      .http
      .get(`${this.uri}`);
  }

  editGroup(id) {
    return this
      .http
      .get(`${this.uri}/edit/${id}`);
  }

  updateGroup(group_name, group_messages, params) {
    const obj = {
      name: group_name,
      messages: group_messages
    };
    console.log(params);
    console.log(obj);
    console.log(`${this.uri}/update/` + params, obj);
    this.http.post(`${this.uri}/update/` + params, obj, { responseType: 'text' }).subscribe(res => console.log('done'));
  }
}
