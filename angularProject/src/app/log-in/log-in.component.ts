import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Account } from '../Models/Account';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../Services/DataService';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  angForm: FormGroup;

    constructor(private ds: DataService, private router: Router, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      username: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  LogIn(username) {
    let user: Account = new Account;
      user.username = username;
    this.ds.connectedUser = user;
    this.ds.sendCurrentGroup("default");

    console.log("logIN");

      this.router.navigate(['principal']);
  }

}
