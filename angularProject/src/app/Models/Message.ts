export class Message {
  sender: String;
  content: String;

  constructor(sender: string, content: string) {
    this.sender = sender;
    this.content = content;
  }
}
