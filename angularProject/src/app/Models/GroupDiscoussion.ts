import { Message } from './Message';

export class GroupDiscussion {
  _id: string;
  name: string;
  messages: Message[];

  constructor(name: string, messages: Message[]) {
    this.name = name;
    this.messages = messages;
  }
}
