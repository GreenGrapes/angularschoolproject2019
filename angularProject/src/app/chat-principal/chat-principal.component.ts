import { Component, OnInit } from '@angular/core';
import { GroupDiscussion } from '../Models/GroupDiscoussion';
import { DataService } from '../Services/DataService';
import { Message } from '../Models/Message';

@Component({
  selector: 'app-chat-principal',
  templateUrl: './chat-principal.component.html',
  styleUrls: ['./chat-principal.component.css']
})
export class ChatPrincipalComponent implements OnInit {

  username: string;
  groupDiscution: GroupDiscussion;
  currentId: string;
  messagesList: Message[];
  currentName: string = "allo";
    interval: number;

  constructor(private ds: DataService) {

  }

  ngOnInit() {
    console.log(this.currentName);
    this.ds.getCurrentGroup().subscribe((data: name) => {
      this.currentName = data.text;
      console.log(data);
      console.log(this.currentName);
      this.getMessages();
    })
    console.log(this.currentName);
    this.interval = window.setInterval(() => {
    this.getMessages();
    }, 1000);
  }

  getMessages() {
    this.ds
      .getGroup()
      .subscribe((data: GroupDiscussion[]) => {
        this.groupDiscution = data.find(x => x.name == this.currentName);
        this.currentId = data.find(x => x.name == this.currentName)._id;
      });
  }
  sendMessage(messageInput: string) {
    this.groupDiscution.messages.push(new Message(this.ds.connectedUser.username, messageInput))
    this.ds.updateGroup(this.groupDiscution.name, this.groupDiscution.messages, this.currentId);
  }
}



type name = {
  text: string;
}
