// JavaScript source code
const express = require('express');
const app = express();
const groupRoutes = express.Router();
const mongoose = require('mongoose');

// Ajouter :  Business model 
let Group = require('../models/GroupDiscussion');

// Definir le route store route
groupRoutes.route('/add').post(function (req, res) {
  let groupDiscussion = new Group(req.body);
  console.log(groupDiscussion);
  groupDiscussion.save()
    .then(groupDiscussion => {
      res.status(200).json({ 'GroupDiscussion': 'GroupDiscussion in added successfully' });
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

// Definir le route: get data(index ou liste) 
groupRoutes.route('/').get(function (req, res) {
  Group.find(function (err, groups) {
    if (err) {
      console.log(err);
    }
    else {
      res.json(groups);
    }
  });
});

// Defined edit route
groupRoutes.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Group.findById(id, function (err, groups) {
    res.json(groups);
  });
});

//  Defined update route
groupRoutes.route('/update/:id').post(function (req, res) {
  console.log("update");
  console.log(req.params.id);
  mongoose.set('useFindAndModify', false);
  //req.params.id = mongoose.Types.ObjectId(req.params.id);
  //console.log(req.params.id);
  Group.findById(req.params.id, function (err, next, business) {
    console.log("err: " + err);
    console.log("next: " + next);
    if (!next) { 
    console.log("error");
    //return next(new Error('Could not load Document'));
  }
    else {
      console.log("not error");
      next.name = req.body.name;
      next.messages = req.body.messages;

      next.save().then(business => {
        res.json('Update complete');
      })
        .catch(err => {
          res.status(400).send("unable to update the database");
        });
    }
  });
});

// Defined delete | remove | destroy route
groupRoutes.route('/delete/:id').get(function (req, res) {
  Group.findByIdAndRemove({ _id: req.params.id }, function (err, group) {
    console.log(err);
    if (err) res.json(err);
    else res.json('Successfully removed');
  });
});

module.exports = groupRoutes;
