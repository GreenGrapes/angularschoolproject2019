// JavaScript source code
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Define collection and schema for Business
let Message = new Schema({
  sender: {
    type: String
  },
  content: {
    type: String
  }
}, {
  collection: 'message'
});


module.exports = mongoose.model('Message', Message);
