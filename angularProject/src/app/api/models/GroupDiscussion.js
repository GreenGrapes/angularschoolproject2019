// JavaScript source code
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let GroupDiscussion = new Schema({
  //_id: {
  //  type: String
  //},
  name: {
    type: String
  },
  messages: {
    type: Array
  }
}, {
  collection: 'GroupDiscussion'
});


module.exports = mongoose.model('GroupDiscussion', GroupDiscussion);
