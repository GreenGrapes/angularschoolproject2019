import { Component, OnInit } from '@angular/core';
import { DataService } from '../Services/DataService';
import { GroupDiscussion } from '../Models/GroupDiscoussion';
import { Message } from '../Models/Message';

@Component({
  selector: 'app-navigation-selector',
  templateUrl: './navigation-selector.component.html',
  styleUrls: ['./navigation-selector.component.css']
})
export class NavigationSelectorComponent implements OnInit {
  groupList: GroupDiscussion[];

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.getGroups();
  }

  getGroups() {
    this.ds.getGroup().subscribe((data: GroupDiscussion[]) => {
      this.groupList = data;
    });
  }

    Select(name: string ) {
        this.ds.sendCurrentGroup(name);
  }

  Create(name: string) {
    let messages: Message[] = [new Message("System", "this is the start of the group")]
    this.groupList.push(new GroupDiscussion(name, messages));
    this.ds.addGroup(name, messages);
    this.Select(name);
  }
}
