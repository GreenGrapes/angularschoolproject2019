import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from './log-in/log-in.component';
import { PrincipalPageComponent } from './principal-page/principal-page.component'


const routes: Routes = [
  { path: '', component: LogInComponent },
  { path: 'principal', component: PrincipalPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
