import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { LogInComponent } from './log-in/log-in.component';
import { PrincipalPageComponent } from './principal-page/principal-page.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavigationSelectorComponent } from './navigation-selector/navigation-selector.component';
import { DataService } from './Services/DataService';
import { ChatPrincipalComponent } from './chat-principal/chat-principal.component';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    PrincipalPageComponent,
    NavigationSelectorComponent,
    ChatPrincipalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
    providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
