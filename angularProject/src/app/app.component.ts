import { Component } from '@angular/core';
import { Account } from '../app/Models/Account';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularProject';
  static connectedUser: Account;
}
